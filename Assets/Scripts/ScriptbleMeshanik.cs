﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level", menuName = "Level", order = 51)]
public class ScriptbleMeshanik : ScriptableObject
{
    [Header("MECHANICKS:")]
   

    [Range(0, 7)]
    [SerializeField] private int[] SequenceMechanics;
  
    [Header("SETTINGS")]
    [SerializeField] private Texture[] TexturesEye;
    [SerializeField] private GameObject BrushProps;
    [SerializeField] private GameObject DiamondProps;
    [SerializeField] private Texture[] Lens;
    [SerializeField] private Sprite[] LensSprite;
    [SerializeField] private Color32[] Lips;
    [SerializeField] private Color32[] EyeBrows;
    [SerializeField] private Texture EyeLashes;

    public Texture[] GetTexturesEye => TexturesEye;
    public GameObject GetBrushProps => BrushProps;
    public GameObject GetDiamondProps => DiamondProps;
    public Texture[] GetLens => Lens;
    public Sprite[] GetSpriteLens => LensSprite;
    public Color32[] GetLips => Lips;
    public Color32[] GetEyeBrows => EyeBrows;
    public Texture GetEyeLashes => EyeLashes;

    public int[] GetSequenceMeshanics => SequenceMechanics;


}
