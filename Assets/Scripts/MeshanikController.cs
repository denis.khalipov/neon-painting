﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshanikController : Singleton<MeshanikController>
{
    [SerializeField] private GameObject[] Meshaniks;
    [SerializeField] private LevelProgressUI m_LevelProgress;
    private ScriptbleMeshanik CurrentMeshaniks;
    private int CurrentMeshActive = 0;

    public void ActiveMeshanik()
    {
        if (!CurrentMeshaniks)
            m_LevelProgress.SetCurrentMechanics(CharasterController.Instance.GetCurrentMeshankis.GetSequenceMeshanics);
        else
            m_LevelProgress.IncreaseProgress(CurrentMeshActive);

        CurrentMeshaniks = CharasterController.Instance.GetCurrentMeshankis;
        UiController.Instance.MoveDoneButton(false);
        if (CurrentMeshActive < CurrentMeshaniks.GetSequenceMeshanics.Length)
        {
            
            Meshaniks[CurrentMeshaniks.GetSequenceMeshanics[CurrentMeshActive]].SetActive(true);
            if (CurrentMeshActive > 0)
            {
                CloseCurrentOpenMechanisk(CurrentMeshaniks.GetSequenceMeshanics[CurrentMeshActive - 1]);
            }

            if (CurrentMeshaniks.GetSequenceMeshanics[CurrentMeshActive] == 0 && Face.Instance.GetTexturesCount > 1) // если в лице 2 текстуры
            {
                if (Face.Instance.CountTextureActive < 2)
                {
                    if (Face.Instance.CountTextureActive == 1)
                    {
                        CurrentMeshActive++;
                    }
                    Face.Instance.ActiveMeshanick();
                }

            }
            else
            {
                if (CurrentMeshaniks.GetSequenceMeshanics[CurrentMeshActive] == 0)
                {
                    Face.Instance.ActiveMeshanick();
                }
                CurrentMeshActive++;
              
            }
        }
        else
        {
            CharacterAnimator.Instance.SetTrigger(CharacterAnimation.Clap);
            CameraController.Instance.StateNextLevel();
            Debug.Log("Win");
            CloseCurrentOpenMechanisk(CurrentMeshaniks.GetSequenceMeshanics[CurrentMeshActive - 1]);
            CloseAllMeshanick();
        }
    }

    private void CloseCurrentOpenMechanisk(int Index)
    {
        switch (Index)
        {
            case 0: //Face
                if (Face.Instance.CountTextureActive > 0)
                {
                    EyesController.Instance.CloseMechanik();
                }
                break;
            case 1:// Lips
                LipsController.Instance.CloseController();
                break;
            case 2://EyeBrows
                EyeBrowsController.Instance.CloseMechaniksh();
                break;
            case 3://Eyelashes
                EyeLashesController.Instance.CloseMechanick();
                break;
            case 4://BrushProps
                BrushMechanic.Instance.CloseMechanick();
                break;
            case 5://Diamon
                DiamandMechanic.Instance.CloseMechanick();
                break;
            case 6://Diamon
                LensController.Instance.CloseMechanick();
                break;

        }
    }
    

    public void CloseAllMeshanick()
    {
        for (int i = 0; i < Meshaniks.Length; i++)
        {
            Meshaniks[i].SetActive(false);
        }
    }

}
