﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiController : Singleton<UiController>
{
    [SerializeField] private ParticleSystem[] EffectWin;
    [SerializeField] private Button DoneButton;
    [SerializeField] private GameObject SpriteFade;
    [SerializeField] private CanvasGroup StartContainer, NextLevelContainer, LevelProgressContainer;
    [SerializeField] private Button TaptoPlayButton,NextLevelButton;
    [SerializeField] private GameObject FinalSmileContainer;
    [SerializeField] private ParticleSystem EffectWinMechanick;
    private void Start()
    {
        NextLevelButton.onClick.AddListener(CloseNextLevelContainer);
        TaptoPlayButton.onClick.AddListener(StartGame);
        SpriteFade.transform.parent.gameObject.SetActive(true);
        SpriteFade.transform.DOScale(22f, 1).SetEase(Ease.Linear);
    }
    private void StartGame()
    {

        LevelController.Instance.SpawnCharacter();
        StartContainer.blocksRaycasts = false;
        TaptoPlayButton.gameObject.SetActive(false);

    }
    private IEnumerator EffectWinActive()
    {
        for (int i = 0; i < EffectWin.Length; i++)
        {
            EffectWin[i].Play();
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.5f);
        FinalSmileContainer.SetActive(true);


    }
  
    public void NextMechanisk()
    {
        MeshanikController.Instance.ActiveMeshanik();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            SpriteFade.transform.DOScale(0, 1).SetEase(Ease.Linear);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            SpriteFade.transform.DOScale(22f, 1).SetEase(Ease.Linear);
        }
    }
    public void MoveDoneButton(bool _active)
    {
        float PosX = _active == true ? 0 : 200;
        if (_active)
        {
            EffectWinMechanick.Play();
        }
        DoneButton.transform.DOLocalMoveX(PosX, 0.5f);
    }
    public void OpenNextLevelContainer()
    {
        LevelProgressContainer.DOFade(0, 0.3f);
        StartCoroutine(EffectWinActive());
        NextLevelContainer.DOFade(1, 0.5f).OnComplete(() =>
        {
            NextLevelContainer.DOFade(1, 1f).OnComplete(() =>
            {
                NextLevelContainer.blocksRaycasts = true;
            });
        

        });
    }
    public void CloseNextLevelContainer()
    {
        LevelController.Instance.EndAnimation();
        NextLevelContainer.blocksRaycasts = false;
        NextLevelContainer.DOFade(0, 0.2f);
       
    }
    public void NextLevel()
    {
      
        SpriteFade.transform.DOScale(0, 1).SetEase(Ease.Linear).OnComplete(()=> {
            DOTween.KillAll();
            SceneManager.LoadScene(0);
        });
    }
}
