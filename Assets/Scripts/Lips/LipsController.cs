﻿using DG.Tweening;
using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LipsController : Singleton<LipsController>
{
    [SerializeField] private GameObject PaintObject;
    [SerializeField] private Lips LipsObject;
    [SerializeField] private GameObject Pomada;
    Material BlickMaterials;
    Sequence MaterialSequence;
    private Vector3 StartPos;
    //UI
    [SerializeField] private LipsUI UILips;
    [SerializeField] private Color32[] ColorLips;
    private bool ActiveInput = false;
    private void Start()
    {
        ColorLips = CharasterController.Instance.GetCurrentMeshankis.GetLips;
        StartPos = PaintObject.transform.position;
        LipsObject = Lips.Instance;
        LipsObject.gameObject.GetComponent<P3dPaintable>().enabled = true;
        ActiveContainer();
    }

    private void OnDisable()
    {
       
    }
    public void CloseController()
    {
        DOTween.Kill(MaterialSequence);
        MaterialSequence.Kill();
        BlickMaterials.DOColor(new Color32(0, 0, 0, 255), "_EmissionColor", 0f);
        LipsObject.gameObject.GetComponent<P3dPaintable>().enabled = false;
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
    }
    private void Update()
    {
        InputController();
    }
    public void ActiveContainer()
    {
        UILips.ActiveContainer();
        UILips.SetColor(ColorLips);

          BlickMaterials = LipsObject.gameObject.GetComponent<Renderer>().material;
        MaterialSequence = DOTween.Sequence()
            .Append(BlickMaterials.DOColor(new Color32(100, 100, 100, 255), "_EmissionColor", 0.7f)).SetLoops(-1, LoopType.Yoyo);

    }
    public void DiactiveContainer()
    {
        UILips.DeactiveContainer();
    }
    public void ColorLipsActive(int _IndexColor)
    {
        GetComponent<P3dPaintDecal>().Color = ColorLips[_IndexColor];
        Pomada.GetComponent<Renderer>().material.color = ColorLips[_IndexColor];
        ActiveInput = true;
        DiactiveContainer();
    }
    private void InputController()
    {
        if (!ActiveInput)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            CameraController.Instance.MoveCamera(10);
        }

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Vector2 Dir = Vector2.zero;;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;


#elif UNITY_ANDROID

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                   Dir = Input.touches[0].deltaPosition/1000;
            }
#endif
            PaintObject.transform.position = PaintObject.transform.position + (new Vector3(Dir.x, Dir.y, 0));

            LipsObject.CheckWinMechanis();

        }
        if (Input.GetMouseButtonUp(0))
        {
            PaintObject.transform.DOMove(StartPos, 0.3f);
            CameraController.Instance.MoveCamera(12);
        }
        
    }
}
