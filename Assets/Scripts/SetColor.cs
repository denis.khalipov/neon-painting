﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetColor : MonoBehaviour
{
    public float Timer = 0;
    public bool BackGround = false;
    void Start()
    {
        StartCoroutine(SetColors());
    }

    private IEnumerator SetColors()
    {
        yield return new WaitForSeconds(Timer);
        if (BackGround)
        {
            LightController.Instance.SetMaterilasBackGround = GetComponent<Renderer>().material;
        }
        else
        {
            LightController.Instance.SetMaterilas = GetComponent<Renderer>().material;
        }
    }
   

}
