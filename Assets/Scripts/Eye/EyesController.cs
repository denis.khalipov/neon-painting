﻿using DG.Tweening;
using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesController : Singleton<EyesController>
{

    [SerializeField] private SkinnedMeshRenderer[] MeshShape;
    [SerializeField] private GameObject FaceObject;
    private Vector3 StartPos;
    private bool LeftEyeActive = true;
    private bool Win = false;
    private float ZPosStart;
    private bool OpenEye = false;
    private void Start()
    {
        StartPos = transform.position;
        MeshShape = LightController.Instance.GetSkinnerMesh.ToArray();
        ZPosStart = transform.position.z;
        FaceObject = Face.Instance.gameObject;
        LeftEyeActive = false;
    }
  
    public void CloseMechanik()
    {
        Win = true;
        RefreshEye();
        FaceObject.GetComponent<Face>().NextMeshacni();
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
      

    }
    private void OnEnable()
    {
        RefreshEye();
    }

    private void Update()
    {
        if (Win)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            CameraController.Instance.MoveCamera(10);
            AnimatorEyes(false);
        }

            if (Input.GetMouseButton(0))
        {
           
            Vector2 Dir = Vector2.zero;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;


#elif UNITY_ANDROID

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Dir = Input.touches[0].deltaPosition/1000;
            }
#endif

            RaycastHit hit;
            transform.position = transform.position + (new Vector3(Dir.x, Dir.y, 0));
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {
                OpenEye = true;

                Vector3 NewPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                transform.position = Vector3.Lerp(transform.position, NewPos, Time.deltaTime * 10);

                //Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            }
            else
            {

                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, ZPosStart), Time.deltaTime * 10);
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            OpenEye = false;
            RefreshEye();
            CameraController.Instance.MoveCamera(12);
            transform.DOMove(StartPos, 0.3f).OnComplete(()=>{

                LeftEyeActive = false;
            });
        }
        if (!OpenEye)
            return;

        if (transform.position.x > 0 && !LeftEyeActive)
        {
            LeftEyeActive = true;
            AnimatorEyes(false);
        }
        else if (transform.position.x < 0 && LeftEyeActive)
        {
            LeftEyeActive = false;
            AnimatorEyes(true);
        }
    }
    
    private void RefreshEye()
    {
        for (int i = 0; i < MeshShape.Length; i++)
        {

            MeshShape[i].SetBlendShapeWeight(1, 0.3f);

            MeshShape[i].SetBlendShapeWeight(2, 0.3f);
        }
    }
    private void AnimatorEyes(bool IsLeftEye)
    {
        float LeftEye = IsLeftEye ? 100 : 0;
        float RightEye = IsLeftEye ? 0 : 100;
        for (int i = 0; i < MeshShape.Length; i++)
        {
            int NewIndex = i;
            DOTween.To(() => LeftEye, x => LeftEye = x, RightEye, 0.1f).OnUpdate(() => {

                MeshShape[NewIndex].SetBlendShapeWeight(1, LeftEye);
            });
            DOTween.To(() => RightEye, x => RightEye = x, LeftEye, 0.1f).OnUpdate(() => {

                MeshShape[NewIndex].SetBlendShapeWeight(2, RightEye);
            });
        }
    }
}
