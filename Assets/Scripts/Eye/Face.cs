﻿using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face : Singleton<Face>
{
    [SerializeField] private Texture[] Textures;
    [SerializeField] P3dPaintableTexture[] TextureCurrents;
    [SerializeField] private GameObject SpriteEye;
    [SerializeField] private GameObject SpriteEyeSmall;
    public GameObject BoneHead;
    public int CountTextureActive = 0;
    public int GetTexturesCount => Textures.Length;

    private float TimerNextMEchanisk = 0;
    private bool ActiveMechanisk = false;
    
    private void Start()
    {
        LightController.Instance.SetSkinnerMesh = GetComponent<SkinnedMeshRenderer>();
        Textures = CharasterController.Instance.GetCurrentMeshankis.GetTexturesEye;
        TextureCurrents = GetComponents<P3dPaintableTexture>();
        TextureCurrents[0].Texture = Textures[0];
        if (Textures.Length == 2)
        {
            TextureCurrents[1].Texture = Textures[1];
        }
    }
    private void Update()
    {
        if(TimerNextMEchanisk>0 )
        {
            TimerNextMEchanisk -= Time.deltaTime;

        }else if(TimerNextMEchanisk <= 0 && ActiveMechanisk)
        {
            ActiveMechanisk = false;
            UiController.Instance.MoveDoneButton(true);
        }
    }
    public void ActiveMeshanick()
    {
        TimerNextMEchanisk = 5;
        ActiveMechanisk = true;
        if (CountTextureActive == 0)
        {
            CountTextureActive++;
            TextureCurrents[0].enabled = true;
            TextureCurrents[0].MipMaps = P3dPaintableTexture.MipType.On;
            TextureCurrents[0].Color = new Color32(255, 255, 255, 0);
            TextureCurrents[0].Activate();
            SpriteEye.SetActive(true);
            GetComponent<P3dPaintable>().enabled = true;
            GetComponent<MeshUpdate>().enabled = true;
        }
        else
        {
            SpriteEye.SetActive(false);
            CountTextureActive++;
            TextureCurrents[0].enabled = false;
            TextureCurrents[1].enabled = true;
            TextureCurrents[1].Coord = P3dCoord.First;
            TextureCurrents[1].MipMaps = P3dPaintableTexture.MipType.On;
            TextureCurrents[1].Color = new Color32(255, 255, 255, 255);
            TextureCurrents[1].Activate();
            GetComponent<P3dPaintable>().enabled = true;
        }
    }
    public void NextMeshacni()
    {
     
        SpriteEye.SetActive(false);
        SpriteEyeSmall.SetActive(false);
        GetComponent<P3dPaintable>().enabled = false;
    }
   
}
