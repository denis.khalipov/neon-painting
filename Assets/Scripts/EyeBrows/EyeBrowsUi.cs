﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyeBrowsUi : MonoBehaviour
{
    [SerializeField] private CanvasGroup Container;
    [SerializeField] private Image[] ButtonImage;
    // Start is called before the first frame update
    public void ActiveContainer()
    {
        Container.blocksRaycasts = true;
        Container.DOFade(1, 0.3f);
    }
    public void DeactiveContainer()
    {
        Container.blocksRaycasts = false;
        Container.DOFade(0, 0.3f);
    }
    public void SetColor(Color32[] _colors)
    {
        for (int i = 0; i < ButtonImage.Length; i++)
        {
            ButtonImage[i].color = _colors[i];
        }
    }
}
