﻿using DG.Tweening;
using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeBrowsController : Singleton<EyeBrowsController>
{

   
    private float ZPosStart;
    [SerializeField] private Eyebrows EyeBrowsObject;
    [SerializeField] private MeshRenderer ObjectBrush;
    private bool ActiveInput = false;
    Material BlickMaterials;
    Sequence MaterialSequence;
    private Vector3 StartPos;

    //UI
    [SerializeField] private EyeBrowsUi UIBrows;
    [SerializeField] private Color32[] ColorBrows;
  

    public void ActiveContainer()
    {
        ColorBrows = CharasterController.Instance.GetCurrentMeshankis.GetEyeBrows;
        UIBrows.ActiveContainer();
        UIBrows.SetColor(ColorBrows);
        BlickMaterials = EyeBrowsObject.GetComponent<Renderer>().material;
        MaterialSequence = DOTween.Sequence()
            .Append(BlickMaterials.DOColor(new Color32(100, 100, 100, 255), "_EmissionColor", 0.7f)).SetLoops(-1, LoopType.Yoyo);
       
       
    }
    public void DiactiveContainer()
    {
        UIBrows.DeactiveContainer();
        
    }
    public void ColorLipsActive(int _IndexColor)
    {
        GetComponent<P3dPaintSphere>().Color = ColorBrows[_IndexColor];
        ObjectBrush.material.color = ColorBrows[_IndexColor];
        ActiveInput = true;
        DiactiveContainer();
    }
    private void OnDisable()
    {
  
    }
    public void CloseMechaniksh()
    {
        DOTween.Kill(MaterialSequence);
        MaterialSequence.Kill();
        BlickMaterials.DOColor(new Color32(0, 0, 0, 255), "_EmissionColor", 0f);
        EyeBrowsObject.GetComponent<P3dPaintable>().enabled = false;
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
    }
    private void Start()
    {
        StartPos = transform.position;
        ZPosStart = transform.position.z;
        EyeBrowsObject = Eyebrows.Instance;
        EyeBrowsObject.GetComponent<P3dPaintable>().enabled = true;
        ActiveContainer();
    }

    private void OnEnable()
    {

    }
    private void Update()
    {
        InputController();
    }
    private void InputController()
    {
        if (!ActiveInput)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            CameraController.Instance.MoveCamera(10);
        }
        if (Input.GetMouseButton(0))
        {

            Vector2 Dir = Vector2.zero;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;

#elif UNITY_ANDROID
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                 Dir = Input.touches[0].deltaPosition/1000;
            }
#endif
            EyeBrowsObject.CheckWinMechanis();
            RaycastHit hit;
            transform.position = transform.position + (new Vector3(Dir.x, Dir.y, 0));
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {


                Vector3 NewPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                transform.position = Vector3.Lerp(transform.position, NewPos, Time.deltaTime * 10);

                //Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            }
            else
            {

                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, ZPosStart), Time.deltaTime * 10);
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            transform.DOMove(StartPos, 0.3f);
            CameraController.Instance.MoveCamera(12);
        }
    }

}
