﻿using DG.Tweening;
using PaintIn3D.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyebrows : Singleton<Eyebrows>
{
    
    private void Start()
    {
        LightController.Instance.SetSkinnerMesh = GetComponent<SkinnedMeshRenderer>();
        CounterTextures = GetComponent<P3dChannelCounter>();
    }

    public float CountChannelCounter;
    private P3dChannelCounter CounterTextures;
    private bool CheckWinMechanisk = false;
    public void CheckWinMechanis()
    {
        if (CounterTextures.CountA > CountChannelCounter && !CheckWinMechanisk)
        {
            CheckWinMechanisk = true;
            UiController.Instance.MoveDoneButton(true);
        }
    }
}
