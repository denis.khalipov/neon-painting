using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyMove : MonoBehaviour
{

    void Start()
    {
        StartCoroutine(MoveMoney());
    }
    private IEnumerator MoveMoney()
    {
        yield return new WaitForSeconds(Random.Range(0, 0.2f));
        transform.DOMove(new Vector3(transform.position.x + Random.Range(-0.1f, 0.1f), transform.position.y + Random.Range(-0.1f, 0.1f), transform.position.z), 0.6f).OnComplete(() => {

            transform.DOMove(MoneyController.Instance.PosMoveMoney.transform.position, 0.5f).OnComplete(()=> {
                MoneyController.Instance.SetTextMoney = 1;
                Destroy(gameObject);
            });
           
        });
    }

    void Update()
    {
        
    }
}
