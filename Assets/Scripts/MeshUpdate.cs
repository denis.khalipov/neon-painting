﻿using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshUpdate : MonoBehaviour
{
    
    private SkinnedMeshRenderer meshRenderer;
    private  MeshCollider collider;
    
    private void Awake()
    {
        collider = GetComponent<MeshCollider>();
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    private void Start()
    {
        UpdateCollider();
    }

    public void UpdateCollider()
    {
        Mesh colliderMesh = new Mesh();
        meshRenderer.BakeMesh(colliderMesh);
        collider.sharedMesh = null;
        collider.sharedMesh = colliderMesh;
    }
}
