using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyController : Singleton<MoneyController>
{
    [SerializeField] private CanvasGroup Container;
    [SerializeField] private Text TextMoney;
    public GameObject PosMoveMoney;
    private int CountMoney;

    private void Start()
    {
        CountMoney = PlayerPrefs.GetInt("Money", 0);
        TextMoney.text = CountMoney.ToString();
    }
    public int SetTextMoney
    {
        set
        {
            CountMoney += value;
            TextMoney.text = CountMoney.ToString();
            PlayerPrefs.SetInt("Money", CountMoney);
        }
    }

}
