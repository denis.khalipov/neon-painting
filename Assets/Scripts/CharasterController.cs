﻿using DG.Tweening;
using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharasterController : Singleton<CharasterController>
{
    [SerializeField] private ScriptbleMeshanik CurrentMeshaniks;
    [SerializeField] private GameObject[] Hair;
    [SerializeField] private GameObject ClosherObject;
    [SerializeField] private Texture[] TextureClosher;
    public ScriptbleMeshanik GetCurrentMeshankis => CurrentMeshaniks;

    
    private void Start()
    {
        GameObject currentHair = Hair[Random.Range(0, Hair.Length)];
        currentHair.SetActive(true);      
        LightController.Instance.SetMaterilas = currentHair.GetComponent<Renderer>().material;
        ClosherObject.GetComponent<Renderer>().material.mainTexture = TextureClosher[Random.Range(0, TextureClosher.Length)];
    }


}
