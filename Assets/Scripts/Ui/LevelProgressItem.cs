﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using DG.Tweening;



public class LevelProgressItem : MonoBehaviour
{
    [SerializeField] private Image m_CompleteIcon, m_CurrentIcon;

    private Sequence m_AnimSequence;
   

    public void Initialize(MechIconsStruct _data)
    {
        m_CurrentIcon.sprite = _data.CurrentIcon;
    }

    //_isCurrent - true - текущая, false - пройденная, null - следующая
    public void SetCurrentState(bool? _isCurrent = null)
    {
        m_CurrentIcon.enabled = m_CompleteIcon.enabled = false;

        m_AnimSequence?.Kill();
        

        switch (_isCurrent)
        {
            case true:
                m_CurrentIcon.enabled = true;
                m_CurrentIcon.transform.DOScale(Vector3.one * 1.25f, 0.5f);
                break;
            case false:
                m_CompleteIcon.enabled = true;
                m_CurrentIcon.transform.DOScale(Vector3.one, 0.5f);
                break;
            case null:
                m_CurrentIcon.enabled = true;
                m_CurrentIcon.transform.DOScale(Vector3.one, 0.5f);
                break;
            default:
                break;
        }
    }
}
