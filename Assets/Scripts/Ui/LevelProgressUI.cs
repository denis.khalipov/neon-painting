﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;


[Serializable]
public struct MechIconsStruct
{
    public int MechIndex;
    public Sprite CurrentIcon;
}

public class LevelProgressUI : MonoBehaviour
{
    [SerializeField] private MechIconsStruct[] m_MechsData;
    [SerializeField] private LevelProgressItem m_ItemPrefab;
    [SerializeField] private RectTransform m_GridContainer;

    private LevelProgressItem[] m_ProgressSteps;
    private int m_CurrentMechanic = 0;

    private void Start()
    {
        
    }

    public void SetCurrentMechanics(int[] _mechanics)
    {
        m_ProgressSteps = new LevelProgressItem[_mechanics.Length];
        for (int i = 0; i < _mechanics.Length; i++)
        {
            m_ProgressSteps[i] = Instantiate(m_ItemPrefab, m_GridContainer);
            m_ProgressSteps[i].Initialize(m_MechsData.FirstOrDefault(item => item.MechIndex == _mechanics[i]));
        }
        SetItemsProgress();
    }

    public void IncreaseProgress(int _currentMechIndex)
    {
        m_CurrentMechanic = _currentMechIndex;
        SetItemsProgress();
    }

    private void SetItemsProgress()
    {
        
        for (int i = 0; i < m_ProgressSteps.Length; i++)
        {
            bool? isCurrent = null;

            if (m_CurrentMechanic >= i)
                isCurrent = m_CurrentMechanic == i ? true : false;

            m_ProgressSteps[i].SetCurrentState(isCurrent);
        }
    }
}
