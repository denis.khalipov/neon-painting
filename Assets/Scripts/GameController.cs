﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Default,
    Game,
    Win,
    Lose,
    SwitchMechanic
}

public class GameController : Singleton<GameController>
{
    public GameState m_GameState = GameState.Game;

    public GameState GetGameState => m_GameState;

    private void Start()
    {
        Application.targetFrameRate = 60;
    }
}
