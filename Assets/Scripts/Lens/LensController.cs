﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensController : Singleton<LensController>
{
    [SerializeField] private GameObject Lens;
    [SerializeField] private GameObject BrushLesn;
    private bool ActiveInput = false;
    private Vector3 StartPos;
    private float ZPosStart;
    private int CountLens = 0;
    private Texture CurrentLensTexture;
    [SerializeField] private LensUI UILens;
    [SerializeField] private Texture[] LensTextures;
    [SerializeField] private Sprite[] LensSprite;

    private void Start()
    {
        LensTextures = CharasterController.Instance.GetCurrentMeshankis.GetLens;
        LensSprite = CharasterController.Instance.GetCurrentMeshankis.GetSpriteLens;
        StartPos = transform.position;
        ZPosStart = transform.position.z;
        ActiveContainer();
    }
    public void ActiveContainer()
    {
        UILens.ActiveContainer();
        UILens.SetTextures(LensSprite);
    }
    public void DiactiveContainer()
    {
        UILens.DeactiveContainer();

    }
    public void ColorLipsActive(int _IndexColor)
    {
        ActiveInput = true;
        CurrentLensTexture = LensTextures[_IndexColor];
        Lens.GetComponent<Renderer>().material.mainTexture = CurrentLensTexture;
        BrushLesn.SetActive(true);
        DiactiveContainer();
    }

    private void Update()
    {
        InputController();
    }
    public void CloseMechanick()
    {
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
    }
    private void InputController()
    {
        if (!ActiveInput)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            CameraController.Instance.MoveCamera(10);
        }

        if (Input.GetMouseButton(0))
        {

            Vector2 Dir = Vector2.zero;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;

#elif UNITY_ANDROID
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                 Dir = Input.touches[0].deltaPosition/1000;
            }
#endif

            RaycastHit hit;
            transform.position = transform.position + (new Vector3(Dir.x, Dir.y, 0));
            Vector3 NewRayCastpos = new Vector3(Lens.transform.position.x, Lens.transform.position.y, Lens.transform.position.z - 0.1f);
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.up), out hit, Mathf.Infinity))
            {

                if (hit.transform.GetComponent<Lens>())
                {
                    transform.DOMove(hit.transform.GetComponent<Lens>().GetMovePos.position, 0.5f).OnComplete(()=> {

                        hit.transform.GetComponent<Lens>().SetMaterilas(Lens.GetComponent<Renderer>().material);
                        Lens.SetActive(false);
                        transform.DOMove(StartPos, 0.5f).OnComplete(() =>
                        {
                            CountLens++;
                            if (CountLens == 2)
                            {
                                UiController.Instance.MoveDoneButton(true);
                            }
                            else
                            {
                                Lens.SetActive(true);
                                ActiveInput = true;
                            }
                        });
                    });
                    ActiveInput = false;
                }
                Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.up) * hit.distance, Color.yellow);
                
            }
            else
            {

             // transform.position = Vector3.Lerp(transform.position, StartPos, Time.deltaTime * 10);
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 1000, Color.white);
                //Debug.Log("Did not Hit");
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            transform.DOMove(StartPos, 0.3f);
            CameraController.Instance.MoveCamera(12);
        }
    }
}
