﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lens : MonoBehaviour
{
    [SerializeField] private ParticleSystem Effect;

    public Transform GetMovePos => Effect.gameObject.transform;
    public void SetMaterilas(Material _mat)
    {
        GetComponent<Renderer>().material = _mat;
        GetComponent<BoxCollider>().enabled = false;
        Effect.Play();
    }
}
