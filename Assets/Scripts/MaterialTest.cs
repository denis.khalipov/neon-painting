﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class MaterialTest : MonoBehaviour
{
    public Material LagMat;
    public Texture FixTextures;
    private void Awake()
    {
        LagMat.mainTexture = FixTextures;
    }
   
}
