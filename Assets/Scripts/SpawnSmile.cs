using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SpawnSmile : MonoBehaviour
{
    [SerializeField] private GameObject Money;
    [SerializeField] private Sprite[] SpriteSmile;
    [SerializeField] private SpriteRenderer Icon;
    [SerializeField] private ParticleSystem EffectKill;

    private void Start()
    {
        Icon.sprite = SpriteSmile[Random.Range(0, SpriteSmile.Length)];
        StartCoroutine(Active());
    }
    private IEnumerator Active()
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 1f));
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider>().enabled = true;
        Vector2 RadiusRandom = Random.insideUnitCircle.normalized * 0.25f;
        Vector3 Pos = new Vector3(RadiusRandom.x, RadiusRandom.y, 0);
        transform.DOLocalMove(Pos, 1).OnComplete(()=> {

            transform.DOScale(transform.localScale, 0.5f).OnComplete(() =>
            {
                transform.DOScale(transform.localScale * 1.5f, 0.1f).OnComplete(() =>
                {
                    Kill();
                });
            });

           
        });
    }
    public void Kill()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        EffectKill.Play();
        for (int i = 0; i < 4; i++)
        {
            Instantiate(Money, transform.position, Quaternion.identity);
        }
       
    }
}
