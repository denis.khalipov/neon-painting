﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelController : Singleton<LevelController>
{
    [SerializeField] private Transform[] m_Levels;
    [SerializeField] private Transform m_SpawnPosition;
    [SerializeField] private DOTweenPath m_MovePath;

    private Transform m_CurrentLevel;
    private Sequence m_MoveSequence;

    private void Start()
    {
      
    }

    void WPCallback(int waypointIndex)
    {
        if (waypointIndex == 3)
            StartMovementEnd();
    }

    public void SpawnCharacter()
    {
        m_MovePath.GetTween().OnWaypointChange(WPCallback);
        m_MovePath.DOPlay();
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel", 0);
        int convertedIndex = currentLevel - (int)(currentLevel / m_Levels.Length) * m_Levels.Length;
        m_CurrentLevel = m_Levels[convertedIndex];

        Instantiate(m_CurrentLevel, m_SpawnPosition);
   
        CharacterAnimator.Instance.SetTrigger(CharacterAnimation.Walk);
      

    }

    private void StartMovementEnd()
    {
        
        m_MovePath.DOPause();
        CharacterAnimator.Instance.SetTrigger(CharacterAnimation.Seat);
        m_MoveSequence = DOTween.Sequence()
            .Append(m_MovePath.transform.DOLocalRotate(Vector3.up * 180f, 0.5f))
            .OnComplete(() => 
            {
                m_MovePath.transform.position = m_MovePath.wps[2];
                CameraController.Instance.GamePlayState();
            });
    }

    public void EndAnimation()
    {
        GameController.Instance.m_GameState = GameState.Win;
        ////  LightController.Instance.LightOn();
        //CameraController.Instance.DefaultState();
        // CharacterAnimator.Instance.SetTrigger(CharacterAnimation.Walk);
        //m_MoveSequence?.Kill();
        //m_MoveSequence = DOTween.Sequence()
        //    .AppendInterval(1f)
        //    .Append(m_MovePath.transform.DOLocalRotate(Vector3.up * 90f, 0.5f))
        //    .OnComplete(() =>
        //    {


        //        m_MovePath.DOPlay();
        //    });
        UiController.Instance.NextLevel();
        NextLevel();
    }
    public void NextLevel()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel", 0);
        currentLevel++;
        if(currentLevel > m_Levels.Length)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
    }
}
