﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : Singleton<CameraController>
{
    [SerializeField] private float m_GameFOV = 10f, m_DefaultFOV = 90f;
    [SerializeField] private UnityEngine.Rendering.PostProcessing.PostProcessVolume PostProcessing;
    private Sequence m_AnimationSequence;
    [SerializeField] private Camera CameraNotPostPross;
    [SerializeField] private GameObject Studios;
    [SerializeField] private GameObject DiscoStudios;
    public void GamePlayState()
    {
        float CountWight = PostProcessing.weight;

        m_AnimationSequence = DOTween.Sequence()
            .AppendInterval(0.5f)
            
            .Append(Camera.main.DOFieldOfView(m_GameFOV, 1.5f).SetEase(Ease.InFlash).OnStart(()=> {
              
                LightController.Instance.LightOff();
                PostProcessing.isGlobal = true;

                DOTween.To(() => CountWight, x => CountWight = x, 1, 1.5f).OnUpdate(() => {

                    PostProcessing.weight = CountWight;
                });
            }))
            .Join(CameraNotPostPross.DOFieldOfView(m_GameFOV, 1.5f).SetEase(Ease.InFlash))
            .OnComplete(() =>
            {
                
                CharacterAnimator.Instance.SetTrigger(CharacterAnimation.Default);
                MeshanikController.Instance.ActiveMeshanik();
            });
        
    }
    public void MoveCamera(int _count)
    {
        Camera.main.DOFieldOfView(_count, 0.3f).SetEase(Ease.InFlash);
        CameraNotPostPross.DOFieldOfView(_count, 0.3f).SetEase(Ease.InFlash);
    }
    public void StateNextLevel()
    {
        LightController.Instance.DiscoStudio();
        Studios.transform.DOMoveY(8.7f, 0.2f);
        DiscoStudios.SetActive(true);
         m_AnimationSequence = DOTween.Sequence()
            .Append(Camera.main.DOFieldOfView(80, 1f).SetEase(Ease.OutFlash))
             .Join(CameraNotPostPross.DOFieldOfView(80, 1f).SetEase(Ease.InFlash))
            .OnComplete(()=> 
            {
                //  PostProcessing.isGlobal = false;
                UiController.Instance.OpenNextLevelContainer();

            });

    }
    public void DefaultState()
    {
        // PostProcessing.isGlobal = false;
        float CountWight = PostProcessing.weight;
        //LightController.Instance.LightOn();
        m_AnimationSequence = DOTween.Sequence()
            .Append(Camera.main.DOFieldOfView(m_DefaultFOV, 0.5f).SetEase(Ease.OutFlash)).OnStart(()=> {

                //DOTween.To(() => CountWight, x => CountWight = x, 0, 0.5f).OnUpdate(() => {

                //    PostProcessing.weight = CountWight;
                //});
            })
            .Join(CameraNotPostPross.DOFieldOfView(m_DefaultFOV, 0.5f).SetEase(Ease.InFlash))
            .OnComplete(() =>
            {
              

            });

    }
}
