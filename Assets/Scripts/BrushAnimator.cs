﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushAnimator : MonoBehaviour
{
    [SerializeField] private GameObject Brush;
    private Vector3 StartScale,StartPos;

    
    private void Awake()
    {
        StartScale = Brush.transform.localScale;
        StartPos = Brush.transform.localPosition;
    }
    private void OnEnable()
    {
        GameController.Instance.m_GameState = GameState.SwitchMechanic;
        Brush.transform.localPosition = new Vector3(StartPos.x-3, StartPos.y, StartPos.z);

        Brush.transform.DOLocalMove(StartPos, 0.3f).OnComplete(() => {

            GameController.Instance.m_GameState = GameState.Game;
        });
    }
    public void DisableMechanisk(GameObject CloseMech)
    {
        GameController.Instance.m_GameState = GameState.SwitchMechanic;
       
        Brush.transform.DOLocalMoveX(StartPos.x-3f, 0.3f).OnComplete(() => {
           
            CloseMech.SetActive(false);
            
        });

    }
}
