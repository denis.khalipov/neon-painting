﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : Singleton<LightController>
{
    [SerializeField] private List<Material> AllStandartMaterials = new List<Material>();
    [SerializeField] private List<Material> AllBackGroundMaterials = new List<Material>();
    [SerializeField] private Color32 BlackColor, DefaultColor,BackGroundColor,ColorDanseStudio;
   
    [SerializeField] private List<SkinnedMeshRenderer> MeshShape = new List<SkinnedMeshRenderer>();
   
    public SkinnedMeshRenderer SetSkinnerMesh
    {
        set
        {
            MeshShape.Add(value);
            DefaultColor = MeshShape[0].material.color;
        }
    }
    public Material SetMaterilasBackGround
    {
        set { AllBackGroundMaterials.Add(value); }
    }
    public List<SkinnedMeshRenderer> GetSkinnerMesh => MeshShape;
    public Material SetMaterilas
    {
        set { AllStandartMaterials.Add(value); }
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            LightOff();
        }
    }
    public void DiscoStudio()
    {
        for (int i = 0; i < AllStandartMaterials.Count; i++)
        {
            AllStandartMaterials[i].DOColor(ColorDanseStudio, 0.4f);

        }
    }
    public void LightOn()
    {
        for (int i = 0; i < AllStandartMaterials.Count; i++)
        {
            AllStandartMaterials[i].DOColor(DefaultColor, 0.4f);

        }
        for (int i = 0; i < AllBackGroundMaterials.Count; i++)
        {
            AllBackGroundMaterials[i].DOColor(DefaultColor, 0.4f);
        }
       
    }
    public void LightOff()
    {
        for (int i = 0; i < AllStandartMaterials.Count; i++)
        {
            AllStandartMaterials[i].DOColor(BlackColor, 1f);
        }

        for (int i = 0; i < AllBackGroundMaterials.Count; i++)
        {
            AllBackGroundMaterials[i].DOColor(BackGroundColor, 1f);
        }
    }
}
