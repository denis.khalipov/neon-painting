﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushBaseAnimator : MonoBehaviour
{
    [SerializeField] private bool m_IsPlayBaseAnimation = true;
    [SerializeField] protected float m_MaxAngle = 35f;

    private void Update()
    {
        if (GameController.Instance.GetGameState != GameState.Game)
            return;

        Animate();
    }

    protected virtual void Animate()
    {
        Vector3 pos = transform.position;

        float currentCoef = Camera.main.WorldToViewportPoint(pos).x;

        bool isRightPos = currentCoef > 0.5f;
        if (currentCoef < 0.5f)
        {
            currentCoef = 0.4f - currentCoef;
        }
        else
        {
            currentCoef = currentCoef - 0.5f;
        }

        currentCoef = Mathf.Clamp(currentCoef, 0, 0.4f) / 0.4f;

        transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, Vector3.up * 180 + Vector3.up * (isRightPos ? m_MaxAngle : -m_MaxAngle) * currentCoef, Time.deltaTime * 15f);
    }
}
