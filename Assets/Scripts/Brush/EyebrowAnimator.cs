﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyebrowAnimator : BrushBaseAnimator
{
    protected override void Animate()
    {
        float xDir = Input.GetAxis("Horizontal");
            
        Vector3 newRotation = Vector3.zero;

        newRotation.y = Mathf.Abs(xDir) < 0.1f ? 0 : -m_MaxAngle * Mathf.Sign(xDir);

        transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, Vector3.up * 270 + newRotation, Time.deltaTime * 5f);
    }
}
