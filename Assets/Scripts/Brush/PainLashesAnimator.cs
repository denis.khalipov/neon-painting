﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainLashesAnimator : BrushBaseAnimator
{
    protected override void Animate()
    {
        float currentCoef = Camera.main.WorldToViewportPoint(transform.position).y;

        currentCoef = (currentCoef - 0.075f) / 0.5f;
        currentCoef = Mathf.Clamp(currentCoef, 0f, 1f);

        /*if (currentCoef > 0.5f)
            currentCoef = 1f;
        else
        {
            currentCoef = Mathf.Clamp(currentCoef - 0.2f, 0f, 0.5f);
            currentCoef = currentCoef / 0.3f;
        }*/

        transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, Vector3.forward * m_MaxAngle * currentCoef, Time.deltaTime * 10f);
    }
}
