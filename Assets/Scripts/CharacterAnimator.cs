﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CharacterAnimation
{
    Seat,
    Walk,
    Default,
    Clap
}

public class CharacterAnimator : Singleton<CharacterAnimator>
{
    private Animator m_Animator;

    protected override void Awake()
    {
        base.Awake();
        m_Animator = GetComponent<Animator>();
    }

    public void SetTrigger(CharacterAnimation _type)
    {
        string triggerName = "";

        switch (_type)
        {
            case CharacterAnimation.Seat:
                triggerName = "Seat";
                m_Animator.SetBool("IsSeat", true);
                break;
            case CharacterAnimation.Walk:
                triggerName = "Walk";
                m_Animator.SetBool("IsSeat", false);
                break;
            case CharacterAnimation.Default:
                triggerName = "Default";
                break;
            case CharacterAnimation.Clap:
                triggerName = "Clap";
                m_Animator.SetBool("Clap", true);
                break;
            default:
                break;
        }

        m_Animator.SetTrigger(triggerName);
    }
}
