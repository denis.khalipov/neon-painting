﻿using PaintIn3D;
using PaintIn3D.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyelashes : Singleton<Eyelashes>
{
    [SerializeField] private Texture Textures;
    private P3dPaintableTexture TextureCurrents;


    private void Start()
    {
        LightController.Instance.SetSkinnerMesh = GetComponent<SkinnedMeshRenderer>();
        CounterTextures = GetComponent<P3dChannelCounter>();
    }
    public void ActiveMeshanick()
    {
        Textures = CharasterController.Instance.GetCurrentMeshankis.GetEyeLashes;
        TextureCurrents = GetComponent<P3dPaintableTexture>();
        TextureCurrents.enabled = true;
        // TextureCurrents[0].Slot = new P3dSlot(1, "_MainTex");
        TextureCurrents.Texture = Textures;
        GetComponent<P3dPaintable>().enabled = true;
    }
    public void NextMeshacni()
    {
        GetComponent<P3dPaintable>().enabled = false;
    }


    public float CountChannelCounter;
    private P3dChannelCounter CounterTextures;
    private bool CheckWinMechanisk = false;
    public void CheckWinMechanis()
    {
        if (CounterTextures.CountA > CountChannelCounter && !CheckWinMechanisk)
        {
            CheckWinMechanisk = true;
            UiController.Instance.MoveDoneButton(true);
        }
    }
}
