﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeLashesController : Singleton<EyeLashesController>
{


    [SerializeField] private Eyelashes EyelashesObject;
    private bool LeftEyeActive = true;
    private bool Win = false;
    private float ZPosStart;
    private Vector3 StartPos;
    Material BlickMaterials;
    Sequence MaterialSequence;
    private void Start()
    {
        StartPos = transform.position;
        ZPosStart = transform.position.z;
        EyelashesObject = Eyelashes.Instance;
        EyelashesObject.GetComponent<Eyelashes>().ActiveMeshanick();

        BlickMaterials = EyelashesObject.GetComponent<Renderer>().material;
        MaterialSequence = DOTween.Sequence()
            .Append(BlickMaterials.DOColor(new Color32(100, 100, 100, 255), "_EmissionColor", 0.7f)).SetLoops(-1, LoopType.Yoyo);

    }
    private void OnDisable()
    {
        //DOTween.Kill(MaterialSequence);
        //MaterialSequence.Kill();
        //BlickMaterials.DOColor(new Color32(0, 0, 0, 255), "_EmissionColor", 0f);
        //EyelashesObject.GetComponent<Eyelashes>().NextMeshacni();
      
    }

    public void CloseMechanick()
    {
        DOTween.Kill(MaterialSequence);
        MaterialSequence.Kill();
        BlickMaterials.DOColor(new Color32(0, 0, 0, 255), "_EmissionColor", 0f);
        EyelashesObject.GetComponent<Eyelashes>().NextMeshacni();
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);

    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CameraController.Instance.MoveCamera(10);
        }
            if (Input.GetMouseButton(0))
        {

            Vector2 Dir = Vector2.zero;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;


#elif UNITY_ANDROID
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                 Dir = Input.touches[0].deltaPosition/1000;
            }
#endif
            EyelashesObject.CheckWinMechanis();
            RaycastHit hit;
            transform.position = transform.position + (new Vector3(Dir.x, Dir.y, 0));
            if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {


                Vector3 NewPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                transform.position = Vector3.Lerp(transform.position, NewPos, Time.deltaTime * 10);

                //Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                //Debug.Log("Did Hit");
            }
            else
            {

                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, ZPosStart), Time.deltaTime * 10);
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                //Debug.Log("Did not Hit");
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            transform.DOMove(StartPos, 0.3f);
            CameraController.Instance.MoveCamera(12);
        }
    }
}
