﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DiamandObject : MonoBehaviour
{
    [SerializeField] private GameObject m_PromtPrefab;

    private SphereCollider m_Collider;
    private MeshRenderer m_MR;
    private Vector3 m_DefaultPosition;
    private Quaternion m_DefaultRotation;
    private Sequence m_AnimationSequence, m_PromtSequence;
    private MeshRenderer m_PromptObject;

    private void Awake()
    {
        m_Collider = GetComponent<SphereCollider>();
        m_MR = GetComponent<MeshRenderer>();
        m_MR.enabled = false;
        m_DefaultPosition = transform.localPosition;
        m_DefaultRotation = transform.localRotation;

        m_PromptObject = Instantiate(m_PromtPrefab, transform).GetComponent<MeshRenderer>();

        m_PromtSequence = DOTween.Sequence()
            .Append(m_PromptObject.materials[0].DOFade(0.5f, 0.25f))
            .Append(m_PromptObject.materials[0].DOFade(1f, 0.25f))
            .SetLoops(-1, LoopType.Yoyo);
    }

    private void OnEnable()
    {
        DiamandMechanic.Instance.Action_MechComplete += DisablePromt;
    }

    private void OnDisable()
    {
        DiamandMechanic.Instance.Action_MechComplete -= DisablePromt;
    }

    public void AnimatePoint(Vector3 _startPos)
    {
        m_Collider.enabled = false;
        m_MR.enabled = true;

        transform.position = _startPos;

        m_AnimationSequence = DOTween.Sequence()
            .Append(transform.DOLocalMove(m_DefaultPosition, 0.35f))
            .Join(transform.DOLocalRotateQuaternion(m_DefaultRotation, 0.35f));

        m_PromtSequence?.Kill();

        m_PromtSequence = DOTween.Sequence()
            .Append(m_PromptObject.materials[0].DOFade(0f, 0.25f))
            .OnComplete(() => 
            {
                m_PromptObject.gameObject.SetActive(false);
            });
    }

    private void DisablePromt()
    {
        if (m_PromptObject)
        {
            m_PromtSequence?.Kill();
            m_PromptObject.gameObject.SetActive(false);
        }
    }
}
