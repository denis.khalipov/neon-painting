﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;

public class BrushMechanic : Singleton<BrushMechanic>
{
    public event Action Action_MechComplete;

    [SerializeField] private Transform m_PointToDraw, m_BrushObject;
    [SerializeField] private LayerMask m_PointsLayer, m_FaceLayer;
    [SerializeField] private Transform[] m_PropFrefabs;

    private bool m_IsAnimated = false;
    private Sequence m_AnimationSequence;
    private float m_Timer = 0.35f;
    private int CountProps = 0;
    private void OnEnable()
    {
        Transform currentProp = CharasterController.Instance.GetCurrentMeshankis.GetBrushProps.transform;
       var CurrentProp =  Instantiate(currentProp, Face.Instance.transform);
        CurrentProp.SetParent(Face.Instance.BoneHead.transform);
        
        CountProps =(int)(currentProp.GetComponentsInChildren<PointObject>().Count()*0.5f);

    }

    private void OnDisable()
    {
      
    }
    public void CloseMechanick()
    {
        Action_MechComplete?.Invoke();
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
    }
    private void Update()
    {

        if (Input.GetMouseButton(0))
        {
            m_Timer -= Time.deltaTime;
            if (m_Timer <= 0)
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, transform.InverseTransformDirection(Vector3.down), out hit, Mathf.Infinity, m_FaceLayer))
                {
                    m_Timer = 0.35f;
                    ShowPoints(hit.point);
                }
            }
        }
    }

    private void ShowPoints(Vector3 _center)
    {
        int maxColliders = 5;
        Collider[] hitColliders = new Collider[maxColliders];
        int numColliders = Physics.OverlapSphereNonAlloc(_center, 0.05f, hitColliders, m_PointsLayer);
        if (numColliders > 0)
        {
            if (!m_IsAnimated)
            {
                m_IsAnimated = true;
                m_AnimationSequence = DOTween.Sequence()
                    .Append(m_BrushObject.DOLocalMove(new Vector3(0f, -0.025f, -0.025f), 0.1f).SetEase(Ease.Linear))
                    .Append(m_BrushObject.DOLocalMove(new Vector3(0f, 0.025f, 0.025f), 0.05f).SetEase(Ease.InFlash))
                    .Append(m_BrushObject.DOLocalMove(Vector3.zero, 0.1f).SetEase(Ease.OutFlash))
                    .OnComplete(() => 
                    {
                        m_IsAnimated = false;
                    });
            }
            for (int i = 0; i < numColliders; i++)
            {
                if (hitColliders[i].GetComponent<PointObject>())
                {
                    hitColliders[i].GetComponent<PointObject>().AnimatePoint(m_PointToDraw.position);
                    CountProps--;
                    if(CountProps == 0)
                    {
                        UiController.Instance.MoveDoneButton(true);
                    }
                }
            }
        }
    }
}
