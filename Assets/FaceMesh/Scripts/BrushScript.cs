﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushScript : MonoBehaviour
{
    [SerializeField] private float m_MaxXOffset;

    private Vector3 m_ScreenPoint, m_Offset;
    private Vector3 StartPos;

    private void Start()
    {
        StartPos = transform.position;
    }

    private void Update()
    {
        if (GameController.Instance.m_GameState != GameState.Game)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            m_ScreenPoint = Camera.main.WorldToScreenPoint(transform.position);
            m_Offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_ScreenPoint.z));
            CameraController.Instance.MoveCamera(10);
        }

        if (Input.GetMouseButton(0) )
        {
            Vector3 mousePosition = Input.mousePosition;

            Vector3 curScreenPoint = new Vector3(mousePosition.x, mousePosition.y, m_ScreenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + m_Offset;
            transform.position = Vector3.Lerp(transform.position, curPosition, Time.deltaTime * 50f);
            Vector3 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, -m_MaxXOffset, m_MaxXOffset);
            transform.position = pos;
        }
        if (Input.GetMouseButtonUp(0))
        {
            CameraController.Instance.MoveCamera(12);
            transform.DOMove(StartPos, 0.3f);
        }
    }
}
