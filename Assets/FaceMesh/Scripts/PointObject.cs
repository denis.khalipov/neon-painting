﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PointObject : MonoBehaviour
{
    [SerializeField] private LayerMask m_FaceLayer;
    [SerializeField] private GameObject m_PromtPrefab;

    private SphereCollider m_Collider;
    private MeshRenderer m_MR;
    private Vector3 m_DefaultPosition;
    private Quaternion m_DefaultRotation;
    private Sequence m_AnimationSequence, m_PromtSequence;
    private MeshRenderer m_PromptObject;

    private void Awake()
    {
        RecalculatePosition();
        CreatePromt();
        m_Collider = GetComponent<SphereCollider>();
        m_MR = GetComponent<MeshRenderer>();
        m_MR.enabled = false;
        m_DefaultPosition = transform.localPosition;
        m_DefaultRotation = transform.localRotation;
    }

    private void OnEnable()
    {
        BrushMechanic.Instance.Action_MechComplete += DisablePromt;
    }

    private void OnDisable()
    {
        BrushMechanic.Instance.Action_MechComplete -= DisablePromt;
    }

    private void CreatePromt()
    {
        if (Random.Range(0, 100) > 75)
        {
            m_PromptObject = Instantiate(m_PromtPrefab, transform).GetComponent<MeshRenderer>();

            m_PromtSequence = DOTween.Sequence()
                .AppendInterval(Random.Range(0f, 2f))
                .Append(m_PromptObject.materials[0].DOFade(0.5f, 1.5f).SetEase(Ease.InFlash))
                .AppendInterval(Random.Range(0f, 1f))
                .Append(m_PromptObject.materials[0].DOFade(0f, 1.5f).SetEase(Ease.OutFlash))
                .SetLoops(-1, LoopType.Yoyo);
        }
    }

    private void RecalculatePosition()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position + transform.up * 0.15f, -transform.up, out hit, Mathf.Infinity, m_FaceLayer))
        {
            transform.position = hit.point + transform.up * 0.0015f;
        }
    }

    public void AnimatePoint(Vector3 _startPos)
    {
        m_Collider.enabled = false;
        m_MR.enabled = true;

        transform.position = _startPos;
        m_AnimationSequence = DOTween.Sequence()
            .Append(transform.DOLocalMove(m_DefaultPosition, 0.35f).SetEase(Ease.InFlash))
            .Join(transform.DOLocalRotateQuaternion(m_DefaultRotation, 0.35f));

        if (m_PromptObject)
        {
            m_PromtSequence?.Kill();
            m_PromtSequence = DOTween.Sequence()
               .Append(m_PromptObject.materials[0].DOFade(0f, 0.25f))
               .OnComplete(() =>
               {
                   m_PromptObject.gameObject.SetActive(false);
               });
        }
    }

    private void DisablePromt()
    {
        if (m_PromptObject)
        {
            m_PromtSequence?.Kill();
            m_PromptObject.gameObject.SetActive(false);
        }
    }
}
