﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public class DiamandMechanic : Singleton<DiamandMechanic>
{
    public event Action Action_MechComplete;

    [SerializeField] private Transform m_PointToDraw, m_BrushObject;
    [SerializeField] private LayerMask m_PointsLayer, m_FaceLayer;
    [SerializeField] private Transform[] m_PropFrefabs;

    private bool m_IsAnimated = false;
    private Sequence m_AnimationSequence;
    private int CountProps = 0;
    private void OnEnable()
    {
        Transform currentProp = CharasterController.Instance.GetCurrentMeshankis.GetDiamondProps.transform;
        var CurrentProp = Instantiate(currentProp, Face.Instance.transform);
        CurrentProp.SetParent(Face.Instance.BoneHead.transform);
        CountProps = (int)(currentProp.GetComponentsInChildren<DiamandObject>().Count() * 0.75f);
    }

  
    public void CloseMechanick()
    {
        Action_MechComplete?.Invoke();
        GetComponent<BrushAnimator>().DisableMechanisk(gameObject);
    }
    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.InverseTransformDirection(Vector3.down), out hit, Mathf.Infinity, m_FaceLayer))
        {
            ShowPoints(hit.point);
        }
    }

    private void ShowPoints(Vector3 _center)
    {
        int maxColliders = 3;
        Collider[] hitColliders = new Collider[maxColliders];
        int numColliders = Physics.OverlapSphereNonAlloc(_center, 0.015f, hitColliders, m_PointsLayer);
        if (numColliders > 0)
        {
            if (!m_IsAnimated)
            {
                m_IsAnimated = true;
                m_AnimationSequence = DOTween.Sequence()
                    .Append(m_BrushObject.DOLocalMove(new Vector3(0f, -0.1f, -0.1f), 0.05f).SetEase(Ease.InFlash))
                    .Append(m_BrushObject.DOLocalMove(new Vector3(0f, -0.08f, -0.08f), 0.1f).SetEase(Ease.OutFlash))
                    .OnComplete(() =>
                    {
                        m_IsAnimated = false;
                    });
            }
            for (int i = 0; i < numColliders; i++)
            {
                if (hitColliders[i].GetComponent<DiamandObject>())
                {
                    hitColliders[i].GetComponent<DiamandObject>().AnimatePoint(m_PointToDraw.position);
                    CountProps--;
                    if (CountProps == 0)
                    {
                        UiController.Instance.MoveDoneButton(true);
                    }
                }
            }
        }
    }
}
