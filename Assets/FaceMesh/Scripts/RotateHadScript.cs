﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateHadScript : MonoBehaviour
{    
    [SerializeField] private float m_MaxAngle;

    private Transform m_FaceObject;


    private void Awake()
    {
        m_FaceObject = CharasterController.Instance.transform;
    }

    private void Update()
    {
        if (m_FaceObject)
        {
            Vector3 pos = transform.position;

            float currentCoef = Camera.main.WorldToViewportPoint(pos).x;

            bool isRightPos = currentCoef > 0.5f;
            if (currentCoef < 0.5f)
            {
                currentCoef = 0.4f - currentCoef;
            }
            else
            {
                currentCoef = currentCoef - 0.5f;
            }

            currentCoef = Mathf.Clamp(currentCoef, 0, 0.4f) / 0.4f;

            m_FaceObject.eulerAngles = Vector3.Lerp(m_FaceObject.eulerAngles, Vector3.up * 180 + Vector3.up * (isRightPos ? m_MaxAngle : -m_MaxAngle) * currentCoef, Time.deltaTime * 7.5f);
        }
        else
        {
            m_FaceObject = FindObjectOfType<CharasterController>().transform;
        }
    }
}
