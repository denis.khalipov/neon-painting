﻿using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Decal
{
    public Texture Texture;
    public float Radius;
    public float Opacity;
}
public class DecalMechanic : MonoBehaviour
{
    [SerializeField] private P3dHitNearby m_HN;
    [SerializeField] private P3dPaintDecal m_PD;
    [SerializeField] private float m_MaxXOffset;
    [SerializeField] private Decal[] m_DecalSprites;

    private int m_CurrentSprite = 0;
    private Vector3 m_DefaultPos = Vector3.zero;

    private void Start()
    {
        m_PD.Texture = m_DecalSprites[m_CurrentSprite].Texture;
        m_PD.Radius = m_DecalSprites[m_CurrentSprite].Radius;
        m_PD.Opacity = m_DecalSprites[m_CurrentSprite].Opacity;
        m_DefaultPos = transform.position;
        Face.Instance.GetComponent<P3dPaintable>().enabled = true;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 Dir = Vector2.zero; ;
#if UNITY_EDITOR
            Dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) / 1000;


#elif UNITY_ANDROID

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                   Dir = Input.touches[0].deltaPosition/1000;
            }
#endif
            Raycast();

            transform.position = transform.position + (new Vector3(Dir.x, Dir.y, 0));
            Vector3 pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, -m_MaxXOffset, m_MaxXOffset);
            transform.position = pos;

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            m_HN.ManuallyHitNow();
            m_CurrentSprite++;

            if (m_CurrentSprite >= m_DecalSprites.Length)
            {
                m_PD.enabled = false;
                Face.Instance.GetComponent<P3dPaintable>().enabled = false;
                MeshanikController.Instance.ActiveMeshanik();
                gameObject.SetActive(false);
                //m_CurrentSprite = 0;
            }
            else
            {
                transform.position = m_DefaultPos;
                Raycast();
                m_PD.Texture = m_DecalSprites[m_CurrentSprite].Texture;
                m_PD.Radius = m_DecalSprites[m_CurrentSprite].Radius;
                m_PD.Opacity = m_DecalSprites[m_CurrentSprite].Opacity;
            }
        }
    }

    private void Raycast()
    {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f), transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            Vector3 NewPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            transform.position = Vector3.Lerp(transform.position, NewPos, Time.deltaTime * 10);
        }
        else
        {

            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, -1), Time.deltaTime * 10);
        }
    }
}
